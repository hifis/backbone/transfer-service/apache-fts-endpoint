#!/bin/bash

# This script sets up an inotifywait for the directory given as either the first commandline argument.
# It calls a supplementary python script with varying parameters depending on the filesystem events 
# and either calculates or (re-)moves digests according to the events detected by inotifywait.
# The digests are either saved in a directory given as the second commandline argument.
# rootdir is created if it doesn't exist already.
# AUTHOR: T. Wetzel, tim.wetzel@desy.de, 2021

if [[ ! -z $1 && ! -z $2 ]]; then
    watchdir=$1
    rootdir=$2
else
    echo "Usage: './watcher.bash watchdir rootdir', where watchdir is the directory to be monitored and rootdir is the directory where the digests are to be saved."
    exit
fi

from=''
mkdir -p $rootdir
inotifywait -m -r -e close_write,move,delete $watchdir |
    while read -r dir evts fname; do
        [[ $fname =~ ^\.davfs.* ]] && continue
        case $evts in
            CLOSE_WRITE,CLOSE)
                echo "$dir$fname has been created."
                python3 /home/dir_watcher.py -m modify -f $dir$fname -d $rootdir
                ;;
            MODIFY)
                echo "$dir$fname has been modified."
                python3 /home/dir_watcher.py -m modify -f $dir$fname -d $rootdir
                ;;
            DELETE)
                echo "$dir$fname has been deleted."
                python3 /home/dir_watcher.py -m delete -f $dir$fname -d $rootdir
                ;;
            MOVED_FROM)
                echo "$dir$fname has been moved from..."
                from=$dir$fname
                ;;
            MOVED_TO)
                echo "$dir$fname has been moved to..."
                if [[ ! -z $from ]]; then
                    python3 /home/dir_watcher.py -m move -f $dir$fname -o $from -d $rootdir
                fi
                from=''
                ;;
        esac
    done
