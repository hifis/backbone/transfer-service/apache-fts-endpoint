#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
# install fail2ban & apache packages
sudo apt-get update && \
sudo apt-get install -y fail2ban apache2 apache2-bin apache2-dev libapache2-mod-auth-openidc git zlib1g-dev libcap-dev inotify-tools

sudo apache2ctl stop
cp envvars /etc/apache2/envvars

# create directories for modified modules, copy files & compile, install and activte modified modules
git submodule init
git submodule update
cd mod_want_digest && sudo apxs -cia mod_want_digest.c
cd ..

curl -O http://mpm-itk.sesse.net/mpm-itk-2.4.7-04.tar.gz && tar xzf mpm-itk-2.4.7-04.tar.gz
cp mpm_itk.c mpm-itk-2.4.7-04/mpm_itk.c
cd mpm-itk-2.4.7-04 && ./configure && make && sudo make install
cd ..

# copy config and load file for mpm_itk
sudo cp mpm_itk.load /etc/apache2/mods-available/mpm_itk.load
sudo cp mpm_itk.conf /etc/apache2/mods-available/mpm_itk.conf
# disable mpm_event because of conflict with mpm_itk
sudo a2dismod mpm_event
# activate apache modules needed
sudo a2enmod rewrite dav dav_fs dav_lock lua mpm_itk mpm_prefork

# copy template of openidc.conf, the values will be replaced by start.sh
sudo cp openidc.conf /etc/apache2/conf-available/openidc.conf

# copy port- and site-configs
sudo cp ports.conf /etc/apache2/ports.conf
sudo cp 000-default.conf /etc/apache2/sites-available/000-default.conf
sudo cp default-ssl.conf /etc/apache2/sites-available/default-ssl.conf

# copying lua script for local user mapping.
sudo mkdir -p /var/run/apache2
sudo cp auth_checker.lua /var/run/apache2/auth_checker.lua
## place userfile.csv
#sudo cp userfile.csv /var/run/apache2/userfile.csv

# copy watcher.bash and dir_watcher.py into the according dir
sudo cp watcher.bash /var/run/apache2/watcher.bash
sudo chmod +x /var/run/apache2/watcher.bash
sudo cp dir_watcher.py /var/run/apache2/dir_watcher.py
sudo chmod +x /var/run/apache2/dir_watcher.py

# copy startup script
sudo cp start.sh /var/run/apache2/start.sh
sudo chmod +x /var/run/apache2/start.sh

# source the apache endpoint variables. you need to fill them in first!
echo "First part of the installation is complete. Please populate the the values in 'apache_ep_env' before starting the server with first_start.sh"
# then execute start.sh
